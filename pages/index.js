import DefaultLayout from "@layouts/default";
import Callback from "@includes/callback";
import Link from "next/link";
import { getConfig, getAllPosts } from "@api";

export default function Frontpage(props) {
  return (
    <div className="containertrans">
      <div className="container">
        <DefaultLayout title={props.title} description={props.description}>
          <h1>Systems Design Experts</h1>
          <p>
            At Spicule we have make our living designing the most effective
            solutions to the infrastructure problems at hand.
          </p>
          <div className="flexwrapper">
            <div className="flexLeft">
              <img className="headerLeft" src="/curiosity.jpeg" />
            </div>
            <div className="flexRight">
              <p className="notopmargin">
                Supporting US Government Space operations, International
                Charities and Medical Research facilities, amongst others, we
                have a wealth of knowledge on tap to help clients large and
                small with their systems design and infrastructure projects.
              </p>
              <p>
                Headquartered in the UK but with an international team, we are
                able to support customers around the globe either for short term
                engagements, or 24/365 support.
              </p>
              <p>We can help with a range of problems, be it:</p>
              <ul>
                <li>Infrastructure design</li>
                <li>Software build, test and deployment</li>
                <li>Data processing pipelines and more.</li>
              </ul>
              <p>
                If any of this sounds familiar and you're having problems in
                house, give us a call!
              </p>
            </div>
          </div>
          <Callback />
          <p>Our latest blog posts:</p>
          <ul>
            {props.posts.map(function (post, idx) {
              return (
                <li key={idx}>
                  <Link href={"/posts/" + post.slug}>
                    <a>{post.title}</a>
                  </Link>
                </li>
              );
            })}
          </ul>
        </DefaultLayout>
      </div>
    </div>
  );
}

export async function getStaticProps() {
  const config = await getConfig();
  const allPosts = await getAllPosts();
  return {
    props: {
      posts: allPosts,
      title: config.title,
      description: config.description,
    },
  };
}
