import '../styles.css'
import { FormspreeProvider } from '@formspree/react';

// This default export is required in a new `pages/_app.js` file.
export default function MyApp({ Component, pageProps }) {
  return (
    <FormspreeProvider project="1547827162625605287">
  <Component {...pageProps} />
  </FormspreeProvider>
  )
}
