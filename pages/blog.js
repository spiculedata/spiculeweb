import DefaultLayout from "@layouts/default";
import Ebook from "@includes/ebook";
import ContactForm from "@includes/contact";
import Link from "next/link";
import { getConfig, getAllPosts } from "@api";

export default function Contact(props) {
  return (
    <div className="containertrans">
      <div className="container">
        <DefaultLayout title={props.title} description={props.description}>
          <p>Our latest blog posts:</p>
          <ul>
            {props.posts.map(function (post, idx) {
              return (
                <li key={idx}>
                  <Link href={"/posts/" + post.slug}>
                    <a>{post.title}</a>
                  </Link>
                </li>
              );
            })}
          </ul>
        </DefaultLayout>
      </div>
    </div>
  );
}

export async function getStaticProps() {
  const config = await getConfig();
  const allPosts = await getAllPosts();
  return {
    props: {
      posts: allPosts,
      title: config.title,
      description: config.description,
    },
  };
}
