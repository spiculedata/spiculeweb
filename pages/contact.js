import DefaultLayout from "@layouts/default";
import Ebook from "@includes/ebook";
import ContactForm from "@includes/contact";
import Link from "next/link";
import { getConfig, getAllPosts } from "@api";

export default function Contact(props) {
  return (
    <div className="containertrans">
      <div className="container">
        <DefaultLayout title={props.title} description={props.description}>
          <h2>Get In Touch!</h2>
          <p>
            Questions, comments, or you just want to say Hi!.
            <br />
            Fill in the form and we'll get back in contact with you.
          </p>
          <ContactForm />
        </DefaultLayout>
      </div>
    </div>
  );
}

export async function getStaticProps() {
  const config = await getConfig();
  const allPosts = await getAllPosts();
  return {
    props: {
      posts: allPosts,
      title: config.title,
      description: config.description,
    },
  };
}
