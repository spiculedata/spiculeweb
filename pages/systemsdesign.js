import DefaultLayout from "@layouts/default";
import Ebook from "@includes/ebook";
import ContactForm from "@includes/contact";
import Link from "next/link";
import { getConfig, getAllPosts } from "@api";
import Callback from "@includes/callback";

export default function SystemsDesign(props) {
  return (
    <div className="containertrans">
      <div className="container">
        <DefaultLayout title={props.title} description={props.description}>
          <div>
            <h1>Systems Design</h1>
            <p>
              At Spicule systems design is our bread and butter. Understanding
              how to build computer platforms for processing data, real time
              event streaming or something else. We can help.
            </p>
            <img className="pageBanner" src="/voyagerwide.jpeg" />
            <div className="flexwrapper">
              <div className="flexLeft50">
                <p>
                  We have helped customers process data from Mars Rovers, helped
                  community charities in Los Angeles, crunched telemetry from
                  the Voyager space probes and more. But this isn't all we can
                  do.
                </p>
                <p>
                  We have decades of experience on data sytems both large and
                  small. Design, build and maintenance, we can deliver it all,
                  or provide mentorship and guidance to an existing team that
                  need some extra experience getting their project over the goal
                  line.
                </p>
              </div>
              <div className="flexRight profile1">
                <p className="notopmargin">
                  <h3 className="notopmargin halternate">
                    Profile: Voyager Maintenance
                  </h3>
                  <p>
                    Working with the team original team, we worked to recreate
                    systems that helped process the systems telemetry coming
                    down from the spacecraft.
                  </p>
                  <p>
                    Replacing systems written in C with a new platform written
                    in Go, Python and React, we aimed to create an environment
                    that mimicked the original platform with as much accuracy as
                    possible.
                  </p>
                </p>
              </div>
            </div>
            <div className="callbackBorder">
              <Callback />
            </div>
            <div className="flexwrapper">
              <div className="flexLeft50 profile2">
                <p className="notopmargin">
                  <h3 className="notopmargin">
                    Profile: A Community Hub Charity
                  </h3>
                  <p>
                    Working for a charity in Los Angeles, we delivered an
                    architecture review to help them mordernise their AWS based
                    infrastructure deployment. Helping segregate applications
                    and updating the Continuous Integration platform, we gave
                    them a design that will help them with reduced
                    infrastructure costs, improved uptime and less maintenance
                    required by the Systems Administrators.
                  </p>
                </p>
              </div>
              <div className="flexRight ">
                <p>
                  We don't have any set methods when it comes to designing
                  systems. We believe in designing solutions around the problems
                  and challenges faced by our customers.
                </p>
                <p>
                  We'll listen to the issues you're facing, then lean on our
                  years of knowledge, our network and research and help deliver
                  a solution that is right for you rather than a generic
                  solution that helps but doesn't resolve the problems at hand.
                </p>
              </div>
            </div>
          </div>
        </DefaultLayout>
      </div>
    </div>
  );
}

export async function getStaticProps() {
  const config = await getConfig();
  const allPosts = await getAllPosts();
  return {
    props: {
      posts: allPosts,
      title: config.title,
      description: config.description,
    },
  };
}
