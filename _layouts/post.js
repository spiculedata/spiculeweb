import DefaultLayout from '@layouts/default'
import Head from 'next/head'
import Link from 'next/link'

export default function PostLayout(props) {
  return (
    <div className="containertrans">
    <div className="container">
    <DefaultLayout>
      <Head>
        <title>{props.title}</title>
      </Head>
     
      <article>
      <div className="postImage">
        <img src={props.image}/>
        </div>
        <h1>{props.title}</h1>
        <div dangerouslySetInnerHTML={{__html:props.content}}/>
        <div><Link href='/'><a>Home</a></Link></div> 
      </article>
     
    </DefaultLayout>
    </div>
      </div>
  )
}