module.exports = {
  //  assetPrefix: process.env.NODE_ENV === 'production' ? '/<GITLAB_PROJECT_NAME>' : '',
    //distDir: 'out',
    target: 'serverless',
    webpack: function (config) {
      config.module.rules.push({test:  /\.md$/, use: 'raw-loader'})
      config.module.rules.push({test: /\.yml$/, use: 'raw-loader'})
      return config
    }
  }