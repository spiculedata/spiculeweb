export default function Header() {
  return (
    <header>
      <img src="/spicule_logo.png" alt="Spicule Logo" className="headerLogo" />
      <a href="/">Home</a> &nbsp; | &nbsp;
      <a href="/systemsdesign">Systems Design</a> &nbsp;| &nbsp;
      {/* <a href="/systemsautomation">Systems Automation</a> &nbsp;| &nbsp; */}
      <a href="/blog">Blog</a> &nbsp;| &nbsp;
      <a href="/contact">Get In Touch</a>
    </header>
  );
}
