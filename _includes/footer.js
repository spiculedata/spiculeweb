export default function Footer() {
    return <footer><p>©2020 | Spicule LTD | <a href="mailto:info@spicule.co.uk">info@spicule.co.uk</a></p></footer>
}