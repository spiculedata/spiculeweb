import React from "react";
import { useForm, ValidationError } from "@formspree/react";

export default function ContactForm() {
  const [state, handleSubmit] = useForm("contactForm2");
  if (state.succeeded) {
    return <p>Thanks for the message! We'll be in touch soon.</p>;
  }
    
    return (
      <form
        className="contactForm"  onSubmit={handleSubmit}
      >
        <input type="email" name="email" placeholder="Your Email" size="50" />
        <br />
        <input type="phone" name="phone" placeholder="Your Phone" size="50" />
        <br />
        <textarea
          type="text"
          name="message"
          placeholder="Your Message"
          cols="50"
          rows="10"
        />
        <br />
        <input type="text" name="_gotcha" style={{display:"none"}} />
        <button type="submit" disabled={state.submitting}>
            Submit
          </button>
      </form>
    );
  
}
