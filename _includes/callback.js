import { useForm, ValidationError } from "@formspree/react";

export default function Callback() {
  const [state, handleSubmit] = useForm("contactForm");
  if (state.succeeded) {
    return <p>Thanks, we'll be in touch soon!</p>;
  }
  return (
    <div>
      <h2 className="halternate">Get in touch!</h2>
      <div className="centerForm">
        <form className="contactForm" onSubmit={handleSubmit}>
          <input name="name" placeholder="Your Name" /> &nbsp;
          <input
            type="text"
            name="message"
            placeholder="Your Phone Number"
          />{" "}
          &nbsp;
          <input type="email" name="email" placeholder="Your Email" />
          &nbsp;
          <input type="text" name="_gotcha" style={{display:"none"}} />
          <button type="submit" disabled={state.submitting}>
            Submit
          </button>
        </form>
      </div>
    </div>
  );
}
