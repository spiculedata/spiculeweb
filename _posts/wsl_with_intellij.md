---
title:  "My New Development Environment - WSL & IntelliJ"
image: "/intellijdesktop.jpg"
categories: Development, IntelliJ, Programming, WSL
---

A few years ago I had an intern working with me, and we decided to give WSL a go because I was doing development on Linux and Mac laptops and he rocked up with the latest Windows 10 laptops from Argos. We installed a Linux distro, OpenJDK, Maven all the usual Java development tooling. We then checked out the source code we were developing on and run a test maven compile. It took... ages. I mean, like an eternity. It did compile but it wasn't viable. So we went back to a stock Windows Java toolchain and gave up on WSL within about an hour of starting.

The problem wasn't the WSL experience itself, it was the disk I/O, it was the worst.

Fast forward a few years and I had a new laptop in my office as a way to do home projects, non NASA work projects and also deal with video and photo editing for my Youtube channels and photo sites. So with that in mind, I didn't want to deal with dual booting Windows to Linux and back again for the Adobe suite, and I didn't want to deal with tradition VMs, none of it appealed. I've been tracking the WSL work done by Canonical and Microsoft over the last year or two Hayden does a [great job of advocacy](https://boxofcables.dev/), but hadn't tested it again, and not only was there WSL 1 there was now WSL 2 which offers far more native integration and support, rather than the terrible Linux -> Windows system call API shim that happened in WSL 1. 

So with the new laptop in hand I gave it a shot. Of course my benchmark was compiling my software, so I checked out some code and compiled it. I was impressed, I had Ubuntu, Windows Terminal and my software all setup and running in nearly no time. Knowing I could run stuff, the networking was pretty sane and now WSL had some support for GUI apps I figured I could at least give it an extended run as a multimedia editing machine coupled with a development box for hobby and part time projects.

## A Porsche vs a Ford Focus

So how does Intellij come into this?

For years I've been an IntelliJ user, on Mac and Linux its generally just works. But WSL obviously has some Windows native vs WSL app tradeoffs and I wanted to test Microsoft's VS Code as it had WSL support on the Windows side to run stuff inside the WSL container, which I thought was pretty cool.

So for 2 weeks I tested VS Code, but stuff just didn't work. Code completion for different languages failed, plugins were clunky. It felt like I'd just downgraded from a Porsche to a Ford Focus. I mean it wasn't like the worst experience ever, but things took time for me to figure out, fix, stop errors, restart and so on. It wasn't a good use of my time for project stuff, its not my day job, where I might be getting paid to fix stuff, I'm doing these projects because I enjoy coding, not fixing IDE's.

So, how can I make development smooth again? How can I get back to where I was when I was doing this on a native Linux box or OSX?

## How do I get IntelliJ running in WSL2?

First up I needed a Linux distribution and X server, there are a few of both available. I use Ubuntu for nearly all my cloud and desktop work, so having an Ubuntu distro which gives me what I'm used to on native Linux but in WSL was great, even with a few tweaks to get Snaps and Juju running. 

I'd been told X410 was worth the £5 or whatever it cost in the Marketplace so I figured it was worth getting. So I got X410 installed and tried running what everyone should run on an X server, xeyes. It loaded fine, with no major changes to the default installation so that was a good start.

Next I grabbed the IntelliJ tarball and unzipped it and launched it with this command:

     export DISPLAY=$(cat /etc/resolv.conf | grep nameserver | awk '{print $2; exit;}'):0.0
      /home/bugg/idea-IU-203.5981.155/bin/idea.sh

To my surprise with minimal effort it sprung to life. 

## What issues did I face?

* It was really small on my HiDPI display.
* File syncing is an issue.
* Popups sometimes hover above all the other windows.

The first one I managed to get to an acceptable point using this blog post: https://intellij-support.jetbrains.com/hc/en-us/articles/360007994999-HiDPI-configuration. It took a bit of tweaking but now I can read the screen at least!

The second two I've not yet really resolved, but they aren't deal breakers. File syncing, used by many IDE's to keep stuff in sync between build chains and the programmers view. Due to the way WSL integrates the file system, IntelliJ doesn't detect changes and so you have to manually sync the filesystem or reopen a file or whatever. Awkwardly sometimes you might edit a file on the command line and then have IntelliJ's autosave then zap the file without it knowing that the actual contents have changed.

The other file issue you face that you have when you're used to IntelliJ on a daily basis is the auto file saving. Usually when you switch focus the file is auto saved so you can go to a console and type your command or whatever. My guess is that IntelliJ doesn't get a defocus notification and so doesn't trigger the save. My solution to this was to set the autosave timer to 2 seconds if you stop typing, that way by the time you enter your console command its saved the file okay. Otherwise getting used to hitting Ctrl-S is mega confusing when you compile code that hasn't changed on the filesystem but doesn't match whats in your IDE window.

Lastly and possibly least troubling is the IDE overlay help popups they give you using IntelliSense. The problem here is that when you Alt Tab to a different application, the X Server emulation leaves your mouse where it was and so the popup appears whilst the rest of the application is in the background, at which point you have to return to IntelliJ and move your mouse to get rid of the popup.

## Final Thoughts

But! Comparing both IDE's and WSL in general, I set out pondering if I could make this work. I use a Macbook Pro at work for my main development, so could I use a Windows box without too much hassle. VS Code isn't good, for Java development at least its just bad when using it with WSL. But IntelliJ in Linux on WSL is excellent, outside of the various small niggles. WSL works well, its still maturing and lots of fun new stuff is planned, but it works more than well enough for my usages. Coupling that with Docker now using WSL as the backend engine, it makes it, it my mind a more compelling development experience for full stack developers than OSX. The Docker backend on OSX has always been slow and clunky because of the emulation QEmu needs to do.

In my heart I'm still a Linux man, you'll not change that, but some times the Laptop or Desktop operating system just needs to be something else, software can dictate that. Also so much of my work these days is using a computer as a portal into another service somewhere. More often than not its just a wrapper around another server, so operating systems matter less and convenience matters more. I swapped from Linux to OSX years ago because of the work reliance on Webex and Linux's terrible support. I didn't want another Mac, so that left Windows or dual boot as my options and I'm more than happy with Windows and WSL2/Ubuntu. If you had asked me a year ago if I'd ever thought this was an option I would have laughed, but today it just seems normal. So normal in fact, I not only have this running on my Laptop, but I also bought a new Desktop and did the same there.

